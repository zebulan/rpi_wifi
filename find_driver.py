from bs4 import BeautifulSoup
from os import uname
from os.path import exists
from re import compile, match
from requests import get
import sqlite3

DB_FILENAME = 'drivers_db'
DOWNLOAD_URL = 'https://dl.dropboxusercontent.com/u/80256631/'
R_PI_URL = 'https://www.raspberrypi.org/forums/viewtopic.php?p=462982'
OUTPUT = '{} {}'.format
OUTPUT2 = '{} #{}'.format


def create_dictionary(drivers):
    try:
        db = sqlite3.connect(DB_FILENAME)
        cursor = db.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS drivers(
            id INTEGER PRIMARY KEY, version TEXT, filename TEXT)''')
        db.commit()
        cursor.executemany('''INSERT INTO drivers(
            version, filename) VALUES(?, ?)''', drivers.items())
        db.commit()
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()


def scrape_drivers():
    regex = compile(r'\d+\.\d+\.\d+.+tar\.gz')  # All Pi Drivers
    # regex = re.compile(r'\d+\.\d+\.\d+-v7\+.+tar\.gz')  # Pi 2 Model B (v7+)

    r = get(R_PI_URL)
    if not r.status_code == 200:
        raise ConnectionError('Error retrieving website')

    drivers = {}
    soup = BeautifulSoup(r.text, 'lxml')
    for text in soup.find_all('code'):
        for line in text:
            string = str(line)
            if match(regex, string):
                version, *numbers, _, filename = string.split()
                if 'inclusive' in numbers:
                    # 3.6.11+ #371 - #520 inclusive - 8188eu-20130209.tar.gz
                    for num in range(371, 521):
                        drivers[OUTPUT2(version, num)] = filename
                else:
                    for num in numbers:
                        drivers[OUTPUT(version, num.strip(','))] = filename
    return drivers


def find_driver(linux_version):
    try:
        db = sqlite3.connect(DB_FILENAME)
        cursor = db.cursor()
        cursor.execute('SELECT filename FROM drivers WHERE version=?',
                       (linux_version,))
        driver = cursor.fetchone()
        if driver:
            return 'Download URL:\n{}{}'.format(DOWNLOAD_URL, driver[0])
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()

if __name__ == '__main__':
    if not exists(DB_FILENAME):
        raspberry_pi_drivers = scrape_drivers()
        create_dictionary(raspberry_pi_drivers)

    print('>>> Raspberry Pi - Raspbian <<<')
    print('Drivers for TL-WN725N V2 Wifi Dongle\n')

    current_version = uname()
    release = current_version[2]
    version = current_version[3].split()[0]
    current_version = '{} {}'.format(release, version)
    print('Current Linux Version:\n{}\n'.format(current_version))

    while True:
        answer = input('Search for driver with current version (Y/N)? ')
        if answer in 'Yy':
            result = find_driver(current_version)
            if result:
                print(result)
                quit()
            else:
                print('Error! No match: {}\n'.format(current_version))
                break
        elif answer in 'Nn':
            break

    while True:
        answer = input('Enter release and version (ex. 3.12.34+ #725): ')
        result = find_driver(answer.strip())
        if result:
            print(result)
            quit()
        else:
            print('Error! No match >>> {} <<<\n'.format(answer))
