import os
import re
import tarfile

REGEX = re.compile(r'8188eu-v7-201\d{5}\.tar\.gz')

if not os.path.exists('WiFi_Driver'):
    os.mkdir('WiFi_Driver')

for f in os.listdir(os.getcwd()):
    if re.match(REGEX, f):
        with tarfile.open(f, 'r') as tar:
            tar.extractall('WiFi_Driver')
        break
else:
    print('Driver tarball not found in current directory')
